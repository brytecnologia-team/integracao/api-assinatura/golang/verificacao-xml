package verify

import (
	"fmt"
	"log"
	"net/http"
	"verificacao-xml/config"
	"verificacao-xml/mapper"
)

func XmlSignatureVerify() {
	params := map[string]string {
		"nonce": config.SignatureNonce,
		"contentReturn": config.ContentReturn,
		"signatures[0][nonce]": config.SignatureNonce,
	}
	files := map[string]string {
		"signatures[0][content]": config.SignaturePath,
	}
	req, err := NewFormDataRequest(config.UrlXmlVerification, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	VerificationReport(resp)
}

func VerificationReport(response *http.Response){
	if response.StatusCode == 200 {
		xmlVerifyResponse, err := mapper.JsonMapper{}.DecodeToXmlVerifyResponse(response)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(xmlVerifyResponse.Report())
	} else {
		str, err := mapper.JsonMapper{}.DecodeToString(response)
		if err != nil {
			log.Fatal(err)
		}
		log.Fatal(str)
	}
}
