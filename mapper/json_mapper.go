package mapper

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"verificacao-xml/model"
)

type JsonMapper struct {}

func (jsonMapper JsonMapper) DecodeToString(response *http.Response) (string, error){
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func (jsonMapper JsonMapper) DecodeToXmlVerifyResponse(response *http.Response) (*model.XmlVerifyResponse, error) {
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var xmlVerifyResponse model.XmlVerifyResponse
	err = json.Unmarshal(bytes, &xmlVerifyResponse)
	return &xmlVerifyResponse, err
}

