package config

const(
	AccessToken = "<ACCESS_TOKEN>"
)

const(
	UrlXmlVerification = "https://fw2.bry.com.br/api/xml-verification-service/v1/signatures/verify"
)

const(
	SignaturePath = "./resource/signature.xml"
	SignatureNonce = "1"
	ContentReturn = "true"
)
