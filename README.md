# Verificação de Assinaturas XML

Código de exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Golang para verificação de assinaturas XML.

### Tech

O exemplo utiliza as bibliotecas Golang abaixo:
* [Golang Version] - 1.12.9
* [Package net/http] - Package http provides HTTP client and server implementations.
* [Package io/ioutil] - Package ioutil implements some I/O utility functions
* [Package mime/multipart] - Package multipart implements MIME multipart parsing, as defined in RFC 2046.


### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.


**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à verificação da assinatura.

| Variável | Descrição | Arquivo de Configuração |
| ------ | ------ | ------ |
| <ACCESS_TOKEN> | Access Token para o consumo do serviço (JWT). | verify_config.go

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.


**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e execute o programa:
```
go run main.go
```

[Golang Version]: <https://golang.org/dl/>
[Package net/http]: <https://golang.org/pkg/net/http/>
[Package io/ioutil]: <https://golang.org/pkg/io/ioutil/>
[Package mime/multipart]: <https://golang.org/pkg/mime/multipart/>

