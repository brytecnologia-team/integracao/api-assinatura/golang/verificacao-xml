package model

import (
	"strconv"
	"strings"
)

type XmlVerifyResponse struct {
	Nonce int
	VerificationStatus []VerificationStatus
}

type VerificationStatus struct {
	GeneralStatus string
	Nonce int
	SignatureFormat string
}

func (vs VerificationStatus) Report() string {
	sb := strings.Builder{}
	sb.WriteString("\t\tVerification Status: {\n")
	sb.WriteString("\t\t\tNonce: " + strconv.Itoa(vs.Nonce) + "\n")
	sb.WriteString("\t\t\tGeneral Status: " + vs.GeneralStatus + "\n")
	sb.WriteString("\t\t\tSignature Format: " + vs.SignatureFormat + "\n")
	sb.WriteString("\t\t}\n")
	return sb.String()
}


func (xvr XmlVerifyResponse) Report() string {
	sb := strings.Builder{}
	sb.WriteString("Xml Verification Response {\n")
	sb.WriteString("\tNonce:" + strconv.Itoa(xvr.Nonce) + "\n")
	sb.WriteString("\tVerifications: [\n")
	for _, status := range xvr.VerificationStatus {
		sb.WriteString( status.Report())
	}
	sb.WriteString("\t]\n")
	sb.WriteString("}\n")
	return sb.String()
}
