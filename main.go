package main

import (
	"log"
	"verificacao-xml/verify"
)

func main(){
	log.Println("============ XML Verification Request")
	verify.XmlSignatureVerify()
}
